//
//  ChannelViewController.swift
//  AgoraSdkDemo
//
//  Created by Jaspreet Kaur on 06/04/21.
//

import UIKit
import AgoraRtcKit

class ChannelViewController: UIViewController {
    
    static let channelName: String = "Agora Demo"
    static let appId: String = "76a50a5319c64980bf024ac6f01d4879"
    
    /// Using an app without a token requirement in this example.
    static var channelToken: String? = nil
    
    /// Setting to zero will tell Agora to assign one for you
    lazy var userID: UInt = 0
    
    /// Role of the local user, `.audience` by default here.
    var userRole: AgoraClientRole = .audience
    
    /// Creates the AgoraRtcEngineKit, with default role as `.audience` above.
    lazy var agkit: AgoraRtcEngineKit = {
        let engine = AgoraRtcEngineKit.sharedEngine(
            withAppId: ChannelViewController.appId,
            delegate: self
        )
        engine.setChannelProfile(.liveBroadcasting)
        engine.setClientRole(self.userRole)
        return engine
    }()
    
    var hostButton: UIButton?
    var closeButton: UIButton?
    
    var controlContainer: UIView?
    var camButton: UIButton?
    var micButton: UIButton?
    var flipButton: UIButton?
    var beautyButton: UIButton?

    var beautyOptions: AgoraBeautyOptions = {
        let bOpt = AgoraBeautyOptions()
        bOpt.smoothnessLevel = 1
        bOpt.rednessLevel = 0.1
        return bOpt
    }()
    
    /// UIView for holding all the camera feeds from broadcasters.
    var agoraVideoHolder = UIView()
    
    /// IDs of the broadcaster(s)
    var remoteUserIDs: Set<UInt> = []
    
    /// Dictionary to find the Video Canvas for a user by ID.
    var userVideoLookup: [UInt: AgoraRtcVideoCanvas] = [:] {
        didSet {
            reorganiseVideos()
        }
    }
    
    /// Local video UIView, used only when broadcasting
    lazy var localVideoView: UIView = {
        let vview = UIView()
        return vview
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(self.agoraVideoHolder)
        self.agoraVideoHolder.frame = self.view.bounds
        self.agoraVideoHolder.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        if ChannelViewController.channelToken != nil {
            // Fetch token if our current one is empty
//            self.joinChannelWithFetch()
        } else {
            self.joinChannel()
        }
    }
    
    /// Join the pre-configured Agora channel
    func joinChannel() {
        self.agkit.enableVideo()
        self.agkit.joinChannel(
            byToken: ChannelViewController.channelToken,
            channelId: ChannelViewController.channelName,
            info: nil, uid: self.userID
        ) { [weak self] _, uid, _ in
            self?.userID = uid
            // Add button to toggle broadcaster/audience
            self?.getHostButton().isHidden = false
            // Add button to close the view
            self?.getCloseButton().isHidden = false
        }
    }
    
    required init() {
        super.init(nibName: nil, bundle: nil)
        self.modalPresentationStyle = .fullScreen
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Add Close and Host buttons to the scene
    
    @discardableResult
    func getCloseButton() -> UIButton {
        if let closeButton = self.closeButton {
            return closeButton
        }
        guard let chevronSymbol = UIImage(systemName: "chevron.left") else {
            fatalError("Could not create chevron.left symbol")
        }
        let button = UIButton.systemButton(with: chevronSymbol, target: self, action: #selector(leaveChannel))
        self.view.addSubview(button)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        [
            button.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 20),
            button.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20)
        ].forEach { $0.isActive = true}
        self.closeButton = button
        return button
    }
    
    /// Leave the Agora channel and return to the main screen
    @objc func leaveChannel() {
        self.agkit.setupLocalVideo(nil)
        self.agkit.leaveChannel(nil)
        if self.userRole == .broadcaster {
            agkit.stopPreview()
        }
        AgoraRtcEngineKit.destroy()
        self.dismiss(animated: true)
    }
    
    /// Shuffle around the videos if multiple people are hosting.
    func reorganiseVideos() {
        if userVideoLookup.isEmpty {
            return
        }
        let vidCounts = userVideoLookup.count

        // Always applying an NxN grid, so if there are 12
        // We take on a grid of 4x4 (showing up to 16 cells).
        let maxSqrt = ceil(sqrt(CGFloat(vidCounts)))
        let multDim = 1 / maxSqrt
        for (idx, (_, canvas)) in userVideoLookup.enumerated() {
            guard let canView = canvas.view else {
                continue
            }
            // A bit hacky, but the next two lines effectively
            // clear the constraints.
            canView.removeFromSuperview()
            self.agoraVideoHolder.addSubview(canView)
            canView.frame.size = CGSize(
                width: self.agoraVideoHolder.bounds.width * multDim,
                height: self.agoraVideoHolder.bounds.height * multDim
            )
            canView.frame.origin = .zero
            if idx % Int(maxSqrt) != 0 {
                // If not the first in the row, find the offset from the left of the view
                canView.frame.origin.x = CGFloat(idx % Int(maxSqrt)) * self.agoraVideoHolder.bounds.width / maxSqrt
            }
            let yMod = Int(CGFloat(idx) / maxSqrt) % Int(maxSqrt)
            if yMod != 0 {
                // If not the first row, find the offset from the top of the view
                canView.frame.origin.y = CGFloat(yMod) * self.agoraVideoHolder.bounds.height / maxSqrt
            }
            canView.autoresizingMask = .all
        }
    }
    
    @discardableResult
    func getHostButton() -> UIButton {
        if let hostButton = self.hostButton {
            return hostButton
        }
        let button = UIButton(type: .custom)
        button.setTitle("Host", for: .normal)
        button.setTitleColor(.label, for: .normal)
        button.setTitleColor(.secondaryLabel, for: .focused)
        button.addTarget(self, action: #selector(toggleBroadcast), for: .touchUpInside)
        self.view.addSubview(button)
        
        button.frame = CGRect(
            origin: CGPoint(x: self.view.bounds.midX - 75, y: 50),
            size: CGSize(width: 150, height: 50)
        )
        button.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleBottomMargin]
        
        button.backgroundColor = .systemRed
        button.layer.cornerRadius = 25
        self.hostButton = button
        return button
    }
    
    /// Toggle between being a host or a member of the audience.
    /// On changing to being a broadcaster, the app first checks
    /// that it has access to both the microphone and camera on the device.
    @objc func toggleBroadcast() {
        // Swap the userRole
        self.userRole = self.userRole == .audience ? .broadcaster : .audience
        
        self.agkit.setClientRole(self.userRole)
    }
}
extension ChannelViewController: AgoraRtcEngineDelegate {

    /// Called when we get a new video feed from a remote user
    /// - Parameters:
    ///   - engine: Agora Engine.
    ///   - uid: ID of the remote user.
    ///   - size: Size of the video feed.
    ///   - elapsed: Time elapsed (ms) from the remote user sharing their video until this callback fired.
    func rtcEngine(_ engine: AgoraRtcEngineKit, firstRemoteVideoDecodedOfUid uid: UInt, size: CGSize, elapsed: Int) {
        // New Remote video feed = new remote broadcaster.
        // Create the video canvas, create a view for the user
        // then add it to the agoraVideoHolder
        let videoCanvas = AgoraRtcVideoCanvas()
        videoCanvas.uid = uid
        let hostingView = UIView()
        self.agoraVideoHolder.addSubview(hostingView)
        videoCanvas.view = hostingView
        videoCanvas.renderMode = .hidden
        self.agkit.setupRemoteVideo(videoCanvas)
        userVideoLookup[uid] = videoCanvas
    }

    /// Called when the user role successfully changes
    /// - Parameters:
    ///   - engine: AgoraRtcEngine of this session.
    ///   - oldRole: Previous role of the user.
    ///   - newRole: New role of the user.
    func rtcEngine(
        _ engine: AgoraRtcEngineKit,
        didClientRoleChanged oldRole: AgoraClientRole,
        newRole: AgoraClientRole
    ) {
        let hostButton = self.getHostButton()
        let isHost = newRole == .broadcaster
        hostButton.backgroundColor = isHost ? .systemGreen : .systemRed
        hostButton.setTitle("Host" + (isHost ? "ing" : ""), for: .normal)
        if isHost {
            self.setupLocalAgoraVideo()
        } else {
            userVideoLookup.removeValue(forKey: self.userID)
        }

        // Only show the camera options when we are broadcasting
        self.getControlContainer().isHidden = !isHost
    }

    /// Setup the canvas and rendering for the device's local video
    func setupLocalAgoraVideo() {
        self.agoraVideoHolder.addSubview(self.localVideoView)
        self.addVideoButtons()
        let videoCanvas = AgoraRtcVideoCanvas()
        videoCanvas.uid = self.userID
        videoCanvas.view = localVideoView
        videoCanvas.renderMode = .hidden
        self.agkit.setupLocalVideo(videoCanvas)
        userVideoLookup[self.userID] = videoCanvas
    }


    func rtcEngine(
        _ engine: AgoraRtcEngineKit,
        didJoinedOfUid uid: UInt,
        elapsed: Int
    ) {
        // Keeping track of all broadcasters in the session
        remoteUserIDs.insert(uid)
    }

    func rtcEngine(
        _ engine: AgoraRtcEngineKit,
        didOfflineOfUid uid: UInt,
        reason: AgoraUserOfflineReason
    ) {
        // Removing on quit and dropped only
        // the other option is `.becomeAudience`,
        // which means it's still relevant.
        if reason == .quit || reason == .dropped {
            remoteUserIDs.remove(uid)
        } else {
            // User is no longer hosting, need to change the lookups
            // and remove this view from the list
            userVideoLookup.removeValue(forKey: uid)
        }
    }
}
extension ChannelViewController {
    /// Add all the relevant buttons.
    /// The buttons are set to add to their respective parent views
    /// Whenever called, so I'm discarding the result for most of them here.
    func addVideoButtons() {
        self.getControlContainer().isHidden = true
        _ = self.getCameraButton()
        _ = self.getMicButton()
        _ = self.getFlipButton()
        _ = self.getBeautifyButton()
    }
    
    func getControlContainer() -> UIView {
        if let controlContainer = self.controlContainer {
            return controlContainer
        }
        let container = UIView()
        self.view.addSubview(container)
        container.frame = CGRect(
            origin: CGPoint(x: 0, y: self.view.bounds.height - 100),
            size: CGSize(width: self.view.bounds.width, height: 100)
        )
        container.autoresizingMask = [.flexibleTopMargin, .flexibleWidth]
        self.controlContainer = container
        return container
    }

    func getCameraButton() -> UIButton {
        if let camButton = self.camButton {
            return camButton
        }
        let container = self.getControlContainer()
        let button = UIButton.newToggleButton(
            unselected: "video", selected: "video.slash"
        )
        button.addTarget(self, action: #selector(toggleCam), for: .touchUpInside)
        container.addSubview(button)

        button.translatesAutoresizingMaskIntoConstraints = false
        [
            button.rightAnchor.constraint(equalTo: container.centerXAnchor, constant: -10),
            button.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -20),
            button.widthAnchor.constraint(equalToConstant: 60),
            button.heightAnchor.constraint(equalToConstant: 60)
        ].forEach { $0.isActive = true }

        button.layer.cornerRadius = 30
        button.backgroundColor = .systemGray
        self.camButton = button
        return button
    }

    func getMicButton() -> UIButton {
        if let micButton = self.micButton {
            return micButton
        }
        let container = self.getControlContainer()
        let button = UIButton.newToggleButton(
            unselected: "mic", selected: "mic.slash"
        )
        button.addTarget(self, action: #selector(toggleMic), for: .touchUpInside)
        container.addSubview(button)

        button.translatesAutoresizingMaskIntoConstraints = false
        [
            button.leftAnchor.constraint(equalTo: container.centerXAnchor, constant: 10),
            button.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -20),
            button.widthAnchor.constraint(equalToConstant: 60),
            button.heightAnchor.constraint(equalToConstant: 60)
        ].forEach { $0.isActive = true }

        button.backgroundColor = .systemGray
        button.layer.cornerRadius = 30
        self.micButton = button
        return button
    }

    func getFlipButton() -> UIButton {
        if let flipButton = self.flipButton {
            return flipButton
        }
        let container = self.getControlContainer()
        let button = UIButton.newToggleButton(unselected: "camera.rotate")
        button.addTarget(self, action: #selector(flipCamera), for: .touchUpInside)
        container.addSubview(button)
        let camButton = self.getCameraButton()

        button.translatesAutoresizingMaskIntoConstraints = false
        [
            button.rightAnchor.constraint(equalTo: camButton.leftAnchor, constant: -20),
            button.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -20),
            button.widthAnchor.constraint(equalToConstant: 60),
            button.heightAnchor.constraint(equalToConstant: 60)
        ].forEach { $0.isActive = true }

        button.backgroundColor = .systemGray
        button.layer.cornerRadius = 30
        self.flipButton = button
        return button
    }

    func getBeautifyButton() -> UIButton {
        if let beautyButton = self.beautyButton {
            return beautyButton
        }
        let container = self.getControlContainer()
        let button = UIButton.newToggleButton(unselected: "wand.and.stars.inverse")
        button.addTarget(self, action: #selector(toggleBeautify), for: .touchUpInside)
        container.addSubview(button)
        let micButton = self.getMicButton()

        button.translatesAutoresizingMaskIntoConstraints = false
        [
            button.leftAnchor.constraint(equalTo: micButton.rightAnchor, constant: 20),
            button.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -20),
            button.widthAnchor.constraint(equalToConstant: 60),
            button.heightAnchor.constraint(equalToConstant: 60)
        ].forEach { $0.isActive = true }

        button.backgroundColor = .systemGray
        button.layer.cornerRadius = 30
        self.beautyButton = button
        return button
    }
    
    /// Toggle the camera between on and off
    @objc func toggleCam() {
        let camButton = self.getCameraButton()
        camButton.isSelected.toggle()
        camButton.backgroundColor = camButton.isSelected ? .systemRed : .systemGray
        self.agkit.enableLocalVideo(!camButton.isSelected)
    }

    /// Toggle the microphone between on and off
    @objc func toggleMic() {
        let micButton = self.getMicButton()
        micButton.isSelected.toggle()
        micButton.backgroundColor = micButton.isSelected ? .systemRed : .systemGray
        self.agkit.muteLocalAudioStream(micButton.isSelected)
    }
    
    /// Turn on/off the 'beautify' effect. Visual and voice change.
    @objc func toggleBeautify() {
        let beautifyButton = self.getBeautifyButton()
        beautifyButton.isSelected.toggle()
        beautifyButton.backgroundColor = beautifyButton.isSelected ? .systemGreen : .systemGray
        //self.agkit.setVoiceBeautifierPreset(
        //    beautifyButton.isSelected ? .timbreTransformationClear : .voiceBeautifierOff
        //)
        self.agkit.setBeautyEffectOptions(beautifyButton.isSelected, options: self.beautyOptions)
    }

    @objc func flipCamera() {
        self.agkit.switchCamera()
    }
}
