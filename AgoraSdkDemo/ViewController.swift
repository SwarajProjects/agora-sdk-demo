//
//  ViewController.swift
//  AgoraSdkDemo
//
//  Created by Jaspreet Kaur on 06/04/21.
//

import UIKit
import AgoraRtcKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func buttonJoinTapped(_ sender: Any) {
        self.present(ChannelViewController(), animated: true)
    }    
}
